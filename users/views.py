from django.contrib.auth.models import User
from rest_framework import viewsets
from .serializers import UserSerializer

from rest_framework.response import Response
from rest_framework.decorators import list_route

from django.contrib.auth import authenticate, login, logout
from rest_framework import status
from rest_framework.exceptions import NotFound, PermissionDenied

from rest_framework.authtoken.models import Token


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @list_route(methods=['post'])
    def login(self, request, pk=None):
        """
        device_id : For only ios and android
        """
        username = request.data.get('email').lower()
        password = request.data.get('password')
        if not (username and password):
            content = {'message': 'Please provide required parameter.'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
        try:
            user_obj = authenticate(
                username=username, password=password)
        except:
            raise NotFound('Something went wong')
        if user_obj:
            login(request, user_obj)
            user = UserSerializer(instance=user_obj)
            data = user.data
            return Response({'user': data})
        else:
            if User.objects.filter(username=username):
                raise NotFound('Invalid Email or Password')
            raise NotFound('User does not exist')

    @list_route(methods=['get'])
    def logout(self, request, pk=None):
        """
        Logout based on the token in the header for logged in user.
        """
        if request.user and request.user.is_authenticated():
            if request.user.device_id:
                request.user.device_id = None
                request.user.save()
            Token.objects.filter(user=request.user).delete()
            logout(request)
            return Response({'message': 'Successfully Logged out'}, status=status.HTTP_200_OK)
        else:
            return Response(status=401)
